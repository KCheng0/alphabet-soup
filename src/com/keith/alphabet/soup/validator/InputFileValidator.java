package com.keith.alphabet.soup.validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Validate the file to ensure the file has a valid format.
 * @author Keith Cheng
 */
public class InputFileValidator {
	private int horizontalRowSize = 0;
	private int verticalColumnSize = 0;
	
	/**
	 * Validate the file and check to see if it is valid.
	 * @param inputFile - input file to check
	 * @return whether the file is valid or not.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public boolean validateFile(File inputFile) throws FileNotFoundException, IOException {
		boolean validFile = true;
		try (BufferedReader br = new BufferedReader(new FileReader(inputFile))){
			String firstLine = br.readLine();
			validFile = validFile && validateFirstLine(firstLine);
			validFile = validFile && validateGrid(br);
		}
		return validFile;
	}
	
	/**
	 * Validate the first line to ensure the user enter 2 number with an 'x' in between
	 * @param firstLine - The first line of the file to check
	 * @return whether the first line is valid
	 */
	private boolean validateFirstLine(String firstLine) {
		String[] wordSearchSize = firstLine.split("x");
		if(wordSearchSize.length != 2) {
			System.out.println("The isn't split by an x or doesn't have exactly 2 parts");
			return false;
		}
		try {
			horizontalRowSize = Integer.parseInt(wordSearchSize[0]);
			verticalColumnSize = Integer.parseInt(wordSearchSize[1]);
		}catch(NumberFormatException e) {
			System.out.println("The first line doesn't have 2 numbers");
			return false;
		}
		return true;
	}
	
	/**
	 * Validate the grid to ensure the grid is in an acceptable format.
	 * @param br - The buffered reader to parse the data
	 * @return whether the grid is valid
	 * @throws IOException
	 */
	private boolean validateGrid(BufferedReader br) throws IOException {
		String nextLine = null;
		int rowNum = 0;
		while(((nextLine = br.readLine()) != null) && rowNum < horizontalRowSize){
			String[] characters = nextLine.split(" ");
			if(characters.length != verticalColumnSize) {
				System.out.println("Number of characters doesn't match the vertical column size input");
				return false;
			}
			for(int columnNum = 0; columnNum < verticalColumnSize; columnNum++) {
				if(characters[columnNum].length() != 1) {
					System.out.println("File is not formatted properly");
					return false;
				}
			}
			rowNum++;
		}
		return true;
	}
}
