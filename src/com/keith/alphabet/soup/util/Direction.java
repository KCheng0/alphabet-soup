package com.keith.alphabet.soup.util;

/**
 * A constant enum file containing the direction the user can search.
 * @author Keith Cheng
 *
 */
public enum Direction {
	NORTH,
	NORTHEAST,
	NORTHWEST,
	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST
}
