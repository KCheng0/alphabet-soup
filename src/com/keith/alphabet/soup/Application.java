package com.keith.alphabet.soup;

import com.keith.alphabet.soup.builder.WordSearchBuilder;
import com.keith.alphabet.soup.object.WordSearch;
import com.keith.alphabet.soup.processor.WordSearchProcessor;
/**
 * Main class to start the application
 * @author Keith Cheng
 */
public class Application {
	public static void main(String[] args) {
		WordSearchBuilder wordSearchBuilder = new WordSearchBuilder();
		WordSearchProcessor processor = new WordSearchProcessor();
		WordSearch wordSearch = wordSearchBuilder.buildWordSearch();
		processor.processWordSearch(wordSearch);
		
	}
}
