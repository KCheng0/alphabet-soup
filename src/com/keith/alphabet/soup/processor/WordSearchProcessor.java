package com.keith.alphabet.soup.processor;

import static com.keith.alphabet.soup.util.Direction.EAST;
import static com.keith.alphabet.soup.util.Direction.NORTH;
import static com.keith.alphabet.soup.util.Direction.NORTHEAST;
import static com.keith.alphabet.soup.util.Direction.NORTHWEST;
import static com.keith.alphabet.soup.util.Direction.SOUTH;
import static com.keith.alphabet.soup.util.Direction.SOUTHEAST;
import static com.keith.alphabet.soup.util.Direction.SOUTHWEST;
import static com.keith.alphabet.soup.util.Direction.WEST;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.keith.alphabet.soup.comparator.WordOrderCompare;
import com.keith.alphabet.soup.object.WordSearch;
import com.keith.alphabet.soup.object.Words;
import com.keith.alphabet.soup.util.Direction;

/**
 * Process the file to parse the data and provide the output in the console
 * @author Keith Cheng
 *
 */
public class WordSearchProcessor {
	
	private List<Words> foundWordList;
	private Map<String, List<Words>> searchMap;
	
	public WordSearchProcessor() {
		foundWordList = new ArrayList<>();
		searchMap = new HashMap<>();
	}
	
	/**
	 * Process the grid by going from left to right and then top to bottom to search for the words to validate. <br>
	 * Afterward sort the output based on the input file and print the result of where the words are.
	 * @param wordSearch to process
	 */
	public void processWordSearch(WordSearch wordSearch) {
		prepareWordForSearch(wordSearch);
		String[][] characterGrid = wordSearch.getCharacterGrid();
		for(int x = 0; x < wordSearch.getHorizontalRowCount(); x++) {
			for(int y = 0; y < wordSearch.getVerticalColumnCount(); y++) {
				String letter = characterGrid[x][y];
				List<Words> searchList = searchMap.get(letter);
				if(searchList != null) {
					List<Words> removeList = new ArrayList<>();
					for(Words searchWord: searchList) {
						if(!searchAround(x, y, searchWord, wordSearch, characterGrid)) {
							foundWordList.add(searchWord);
							removeList.add(searchWord);
						}
					}
					searchList.removeAll(removeList);
				}
			}
		}
		Collections.sort(foundWordList, new WordOrderCompare());
		for(Words foundWord: foundWordList) {
			System.out.println(foundWord);
		}
	}
	
	/**
	 * Prepare the word search to ensure the grid isn't being searched for each word.<br>
	 * Cache the word so if the letter on the grid is the first word then have a list of potential words that can be searched.
	 * @param wordSearch to prepare
	 */
	private void prepareWordForSearch(WordSearch wordSearch) {
		List<Words> searchWords = wordSearch.getWords();
		for(Words word: searchWords) {
			String searchWord = word.getSearchString();
			String firstLetter = searchWord.substring(0, 1);
			List<Words> wordList = searchMap.get(firstLetter);
			if(wordList == null) {
				List<Words> newWordList = new ArrayList<>();
				newWordList.add(word);
				searchMap.put(firstLetter, newWordList);
			}else {
				wordList.add(word);
			}
		}
	}
	
	/**
	 * Perform calculation on whether it's possible to find the word based on how far away from the edge of the grid the word is.
	 * @param x - x coordinate of the letter to search from
	 * @param y - y coordinate of the letter to search from
	 * @param searchWord - the word that is being searched
	 * @param wordSearch - the word search to identify the maximum x and y coordinate
	 * @param characterGrid - the character grid to search on
	 * @return whether the word is found starting from the given x and y coordinate
	 */
	private boolean searchAround(int x, int y, Words searchWord, WordSearch wordSearch, String[][] characterGrid) {
		int wordLength = searchWord.getSearchString().length();
		int rowNum = wordSearch.getHorizontalRowCount();
		int columnNum = wordSearch.getVerticalColumnCount();
		boolean notFound = true;
		if(notFound && x + 1 - wordLength >= 0) {
			notFound = notFound && !validWord(x, y, searchWord, characterGrid, NORTH);
			if(notFound && y + 1 - wordLength >= 0) {
				notFound = notFound && !validWord(x, y, searchWord, characterGrid, NORTHWEST);
			}
			if(notFound && y + wordLength <= columnNum) {
				notFound = notFound && !validWord(x, y, searchWord, characterGrid, NORTHEAST);
			}
		}
		if(notFound && x + wordLength <= rowNum) {
			notFound = notFound && !validWord(x, y, searchWord, characterGrid, SOUTH);
			if(notFound && y + 1 - wordLength >= 0) {
				notFound = notFound && !validWord(x, y, searchWord, characterGrid, SOUTHWEST);
			}
			if(notFound && y + wordLength <= columnNum) {
				notFound = notFound && !validWord(x, y, searchWord, characterGrid, SOUTHEAST);
			}
		}
		if(notFound && y + 1 - wordLength >= 0) {
			notFound = notFound && !validWord(x, y, searchWord, characterGrid, WEST);
		}
		if(notFound && y + wordLength <= columnNum) {
			notFound = notFound && !validWord(x, y, searchWord, characterGrid, EAST);
		}
		return notFound;
	}
	
	/**
	 * Validate if the word exist starting from the x and y coordinate.
	 * @param x - x coordinate of where to start the word search
	 * @param y - y coordinate of where to start the word search
	 * @param searchWord - the word that is being searched
	 * @param characterGrid - the character grid to search on
	 * @param direction - the direction of the search
	 * @return
	 */
	private boolean validWord(int x, int y, Words searchWord, String[][] characterGrid, Direction direction) {
		String searchString = searchWord.getSearchString();
		for(int i= 0; i < searchString.length();i++) {
			switch(direction) {
				case NORTH: {
					if(!characterGrid[x - i][y].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case NORTHEAST: {
					if(!characterGrid[x - i][y + i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case NORTHWEST: {
					if(!characterGrid[x - i][y - i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case SOUTH: {
					if(!characterGrid[x + i][y].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case SOUTHEAST: {
					if(!characterGrid[x + i][y + i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case SOUTHWEST: {
					if(!characterGrid[x + i][y - i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case WEST: {
					if(!characterGrid[x][y - i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
				case EAST: {
					if(!characterGrid[x][y + i].equals(searchString.substring(i, i+1))) {
						return false;
					}
					break;
				}
			}
		}
		fillCoordinate(x, y, direction, searchWord);
		return true;
	}
	
	/**
	 * Fill the coordinate information when the word is found in the grid
	 * @param x - x coordinate of where the word was found
	 * @param y - y coordinate of where the word was found
	 * @param direction - the word was found in
	 * @param searchWord - the word information to fill in
	 */
	private void fillCoordinate(int x, int y, Direction direction, Words searchWord) {
		int wordLength = searchWord.getOriginalString().length();
		searchWord.setVerticalStart(y);
		searchWord.setHorizontalStart(x);
		
		switch(direction) {
			case NORTH: 
				searchWord.setVerticalEnd(y);
				searchWord.setHorizontalEnd(x + 1 - wordLength);
				break;
			case SOUTH:
				searchWord.setVerticalEnd(y);
				searchWord.setHorizontalEnd(x - 1 + wordLength);
				break;
			case EAST:
				searchWord.setVerticalEnd(y - 1 + wordLength);
				searchWord.setHorizontalEnd(x);
				break;
			case WEST:
				searchWord.setVerticalEnd(y + 1 - wordLength);
				searchWord.setHorizontalEnd(x);
				break;
			case NORTHEAST: 
				searchWord.setVerticalEnd(y - 1 + wordLength);
				searchWord.setHorizontalEnd(x + 1 - wordLength);
				break;
			case NORTHWEST:
				searchWord.setVerticalEnd(y + 1 - wordLength);
				searchWord.setHorizontalEnd(x + 1 - wordLength);
				break;
			case SOUTHEAST:
				searchWord.setVerticalEnd(y -1 + wordLength);
				searchWord.setHorizontalEnd(x -1 + wordLength);
				break;
			case SOUTHWEST:
				searchWord.setVerticalEnd(y + 1 -  wordLength);
				searchWord.setHorizontalEnd(x - 1 + wordLength);
				break;
		}
	}
}
