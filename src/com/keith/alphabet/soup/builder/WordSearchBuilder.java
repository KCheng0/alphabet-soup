package com.keith.alphabet.soup.builder;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import com.keith.alphabet.soup.object.WordSearch;
import com.keith.alphabet.soup.parser.InputFileParser;
import com.keith.alphabet.soup.validator.InputFileValidator;

/**
 * The class containing logic to check if the file exist, validate the data in the file, and turn it into a word search object..
 * @author Keith Cheng
 */
public class WordSearchBuilder {
	
	/**
	 * Ask the user for the path of the file on their system and then parse the file into usable data.
	 * @return word search to search on.
	 */
	public WordSearch buildWordSearch() {
		File inputFile = getInputFileAndValidate();
		WordSearch wordSearch = parseInputFile(inputFile);
		return wordSearch;
	}
	
	/**
	 * Ask the user for the file path and ensure it exist as well as ensuring the file is in the expected format.
	 * @return the valid file.
	 */
	private File getInputFileAndValidate() {
		InputFileValidator inputFileValidator = new InputFileValidator();
		Scanner scanner = new Scanner(System.in);
		boolean validFile = false;
		
		System.out.println("Enter the full path of the input file");
		String inputFilePath = scanner.nextLine();
		
		File inputFile = new File(inputFilePath);
		while(!validFile) {
			if(!inputFile.exists()) {
				System.out.println("File does not exist. Please input a full file path for a file that exist");
				inputFilePath = scanner.nextLine();
				inputFile = new File(inputFilePath);
				continue;
			}
			try {
				validFile = inputFileValidator.validateFile(inputFile);
				if(!validFile) {
					System.out.println("Please enter a valid file path");
					inputFilePath = scanner.nextLine();
					inputFile = new File(inputFilePath);
				}
			} catch (IOException e) {
				System.out.println("A problem occurred when reading the file" );
				e.printStackTrace();
				inputFilePath = scanner.nextLine();
				inputFile = new File(inputFilePath);
			}
		}
		scanner.close();
		return inputFile;
	}
	
	/**
	 * Parse the input file into a word search.
	 * @param inputFile - the valid file to turn into a word search
	 * @return WordSearch that is used by the program to determine the x and y coordinate of the words in the grid.
	 */
	private WordSearch parseInputFile(File inputFile) {
		InputFileParser inputFileParser = new InputFileParser();
		WordSearch wordSearch = null;
		try {
			wordSearch = inputFileParser.parseFile(inputFile);
		} catch (IOException e) {
			return null;
		}
		return wordSearch;
	}
}
