package com.keith.alphabet.soup.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.keith.alphabet.soup.object.WordSearch;
import com.keith.alphabet.soup.object.Words;

/**
 * The file input parser to ensure turn the file into a word search.
 * @author K
 *
 */
public class InputFileParser {
	private int horizontalRowSize;
	private int verticalColumnSize;
	private WordSearch wordSearch;
	
	public InputFileParser() {
		this.horizontalRowSize = 0;
		this.verticalColumnSize = 0;
		this.wordSearch = new WordSearch();
	}
	
	/**
	 * Parse the file into a word search object so the application can process the data.
	 * @param inputFile - input file to process
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public WordSearch parseFile(File inputFile) throws FileNotFoundException, IOException {
		
		try (BufferedReader br = new BufferedReader(new FileReader(inputFile))){
			String firstLine = br.readLine();
			parseFirstLine(firstLine);
			parseGrid(br);
			parseWords(br);
		}
		return wordSearch;
	}
	
	
	/**
	 * Parse the first line and save the maximum row and column size
	 * @param firstLine - of the file to parse
	 */
	private void parseFirstLine(String firstLine) {
		String[] wordSearchSize = firstLine.split("x");
		
		horizontalRowSize = Integer.parseInt(wordSearchSize[0]);
		verticalColumnSize = Integer.parseInt(wordSearchSize[1]);
		wordSearch.setHorizontalRowCount(horizontalRowSize);
		wordSearch.setVerticalColumnCount(verticalColumnSize);
	}
	
	/**
	 * Parse the grid portion to turn the grid into a double array list.
	 * @param br - The buffered reader containing the file data.
	 * @throws IOException
	 */
	private void parseGrid(BufferedReader br) throws IOException {
		String nextLine = null;
		int rowNum = 0;
		int columnNum = 0;
		String[][] grid = new String[horizontalRowSize][verticalColumnSize];
		while(rowNum < horizontalRowSize && ((nextLine = br.readLine()) != null)){
			String rowString = nextLine.toUpperCase();
			String[] row = rowString.split(" ");
			for(String character: row) {
				grid[rowNum][columnNum] = character;
				columnNum++;
			}
			columnNum = 0;
			rowNum++;
		}
		wordSearch.setCharacterGrid(grid);
	}
	
	
	/**
	 * Parse the words in the file to save both the search string and original string.
	 * @param br - The buffered reader containing the remaining file data.
	 * @throws IOException
	 */
	private void parseWords(BufferedReader br) throws IOException {
		String nextLine = null;
		List<Words> wordList = new ArrayList<>();
		int wordOrder = 0;
		while((nextLine = br.readLine()) != null) {
			Words word = new Words();
			String searchWord = nextLine.replaceAll("\\s", "").toUpperCase();
			if(searchWord.isEmpty()) {
				continue;
			}
			word.setOriginalString(nextLine);
			word.setSearchString(searchWord);
			word.setWordOrder(wordOrder);
			wordOrder++;
			wordList.add(word);
		}
		wordSearch.setWords(wordList);
	}
}
