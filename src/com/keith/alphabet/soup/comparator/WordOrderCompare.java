package com.keith.alphabet.soup.comparator;

import java.util.Comparator;

import com.keith.alphabet.soup.object.Words;

/**
 * A comparator to compare words to ensure they are sorted in the expected order before it is displayed to the user.
 * @author Keith Cheng
 *
 */
public class WordOrderCompare implements Comparator<Words>{

	@Override
	public int compare(Words o1, Words o2) {
		return o1.getWordOrder().compareTo(o2.getWordOrder());
	}
	
}
