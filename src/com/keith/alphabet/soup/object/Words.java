package com.keith.alphabet.soup.object;

/**
 * The word object that contains various information to be used for display.
 * @author Keith Cheng
 *
 */
public class Words{
	private String originalString;
	private String searchString;
	private int horizontalStart;
	private int horizontalEnd;
	private int verticalStart;
	private int verticalEnd;
	private Integer wordOrder;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(originalString);
		sb.append(" ");
		sb.append(horizontalStart);
		sb.append(":");
		sb.append(verticalStart);
		sb.append(" ");
		sb.append(horizontalEnd);
		sb.append(":");
		sb.append(verticalEnd);
		return sb.toString();
	}
	
	public String getOriginalString() {
		return originalString;
	}
	
	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}
	
	public String getSearchString() {
		return searchString;
	}
	
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	
	public int getHorizontalStart() {
		return horizontalStart;
	}
	
	public void setHorizontalStart(int horizontalStart) {
		this.horizontalStart = horizontalStart;
	}
	
	public int getHorizontalEnd() {
		return horizontalEnd;
	}
	
	public void setHorizontalEnd(int horizontalEnd) {
		this.horizontalEnd = horizontalEnd;
	}
	
	public int getVerticalStart() {
		return verticalStart;
	}
	
	public void setVerticalStart(int verticalStart) {
		this.verticalStart = verticalStart;
	}
	
	public int getVerticalEnd() {
		return verticalEnd;
	}
	
	public void setVerticalEnd(int verticalEnd) {
		this.verticalEnd = verticalEnd;
	}
	
	public Integer getWordOrder() {
		return wordOrder;
	}
	
	public void setWordOrder(Integer wordOrder) {
		this.wordOrder = wordOrder;
	}

}
