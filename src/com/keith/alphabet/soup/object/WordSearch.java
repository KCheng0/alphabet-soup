package com.keith.alphabet.soup.object;

import java.util.List;

/**
 * The word search board containing the grid, word, and board size.
 * @author Keith Cheng
 *
 */
public class WordSearch {
	private String[][] characterGrid;
	private int horizontalRowCount;
	private int verticalColumnCount;
	private List<Words> words;

	public String[][] getCharacterGrid() {
		return characterGrid;
	}

	public void setCharacterGrid(String[][] characterGrid) {
		this.characterGrid = characterGrid;
	}

	public int getHorizontalRowCount() {
		return horizontalRowCount;
	}

	public void setHorizontalRowCount(int horizontalRowCount) {
		this.horizontalRowCount = horizontalRowCount;
	}

	public int getVerticalColumnCount() {
		return verticalColumnCount;
	}

	public void setVerticalColumnCount(int verticalColumnCount) {
		this.verticalColumnCount = verticalColumnCount;
	}

	public List<Words> getWords() {
		return words;
	}

	public void setWords(List<Words> words) {
		this.words = words;
	}
	
	
}
